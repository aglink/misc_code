% TO BE PASTED BEFORE TIME LOOP

Eacc_old = zeros(MESH.nel,3);
Eheal_old = zeros(MESH.nel,3);

% TO BE PASTED AFTEL FLOW SOLVER    
try
    if SETTINGS.fract_heal
        dEheal = VAR.Eacc_heal-Eheal_old;
        dEheal(dEheal<0) = 0;
        Eheal_old = VAR.Eacc_heal;
        plot_2d_fedata(77,MESH.gc_nod,MESH.el2nod,log10(dEheal./(dt.*NUMSCALE.t0)));title('dE_{heal}');caxis([-15 -13])
        print(77,'-dpng','-r150',[SETTINGS.workdir '/Plot_dEheal_' num2str_d(istep,5)]);
    else
        dEacc = VAR.Eacc_II-Eacc_old;
        dEacc(dEacc<0) = 0;
        Eacc_old = VAR.Eacc_II;
        plot_2d_fedata(77,MESH.gc_nod,MESH.el2nod,log10(dEacc./(dt.*NUMSCALE.t0)));title('dE_{acc}');caxis([-15 -13])
        print(77,'-dpng','-r150',[SETTINGS.workdir '/Plot_dEacc_' num2str_d(istep,5)]);
    end
catch
    if SETTINGS.fract_heal
        Eheal_old = VAR.Eacc_heal;
    else
        Eacc_old = VAR.Eacc_II;
    end
end
% plot_2d_fedata(78,MESH.gc_nod,MESH.el2nod,log10(VAR.Er_II_totl./(NUMSCALE.t0)));title('E_{r}^{II}');caxis([-15 -13])
% plot_2d_fedata(79,MESH.gc_nod,MESH.el2nod,VAR.Eacc_II);title('E_{acc}');caxis([0 1])
drawnow