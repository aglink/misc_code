function plot_deformation_map(P,T,FigNo)

scaling        = 'mantle';
NUMSCALE       = numerical_eq_scaling(scaling);
RHEOL          = rheology_dis_dif_creep({'olivine_HK'},{'dry'});

C2K = 273.15;
T   = T+C2K;
R   = 8.314472; % J/mol K; univ. gas constant
d   = logspace(0,6,1000);
Er  = logspace(-25,-10,4);

Visc_dif = 1/2 * (RHEOL.Adif .* d.^(-RHEOL.m)).^(-1/RHEOL.Ndif) .* exp((RHEOL.Qdif + RHEOL.Vdif*P)./ (RHEOL.Ndif*R*T));

for i = 1:length(Er)
    Tau = 2*Visc_dif*Er(i);
    
    Visc_dis = 1/2 * RHEOL.Adis.^(-1/RHEOL.Ndis) .* Er(i).^(1/RHEOL.Ndis-1) .* exp((RHEOL.Qdis + RHEOL.Vdis*P)./ (RHEOL.Ndis*R*T));
    Tau(Visc_dif > Visc_dis) = 2*Visc_dis*Er(i);
    
    
    figure(FigNo)
    hold on
    plot(log10(d),log10(Tau./1e6),'k')
end

Tau = logspace(-2,4,1000);
d_vdW(1,:) = 0.015*Tau.^(-1.33)*1e6;
d_vdW(2,:) = 0.0147*Tau.^(-1.42)*1e6;
d_vdW(3,:) = 0.0154*Tau.^(-1.24)*1e6;

figure(FigNo)
hold on
plot(log10(d_vdW(1,:)),log10(Tau),'k')
hold on
plot(log10(d_vdW(2,:)),log10(Tau),'k')
hold on
plot(log10(d_vdW(3,:)),log10(Tau),'k')

xlabel('d [\mum]')
ylabel('\tau [MPa]')
xlim([0 6])
ylim([-2 4])