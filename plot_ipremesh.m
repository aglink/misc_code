function plot_ipremesh

x = linspace(-0.5,0.5,101);
z = linspace(0.5,-0.5,101);
[X,Z] = meshgrid(x,z);
C = Z;

gc_nod1 = 1.5*[-0.5 0.5 0;
           [-0.5 -0.5 0.5]+0.1];
el2nod1 = [1 2 3]';

ind = inpolygon(X(:),Z(:),gc_nod1(1,:),gc_nod1(2,:));
C(~ind) = NaN;

[xg_ip1,zg_ip1] = ip_gcoord_tri(gc_nod1,el2nod1,3);

gc_nod2 = [-1.5 -1.5 0  1.5 1.5;
           -1.5  1.5 0 -1.5 1.5];
el2nod2 = [1 3 2; 1 4 3; 2 3 5; 3 4 5]';
[xg_ip2,zg_ip2] = ip_gcoord_tri(gc_nod2,el2nod2,3);

figure(234)
contour(X,Z,C,'linewidth',2)
hold on
plot_mesh(gc_nod1,el2nod1,234,'k')
hold on
scatter(xg_ip1,zg_ip1,50,zg_ip1','filled')
hold on
plot_mesh(gc_nod2,el2nod2,234,'k')
hold on
scatter(xg_ip2(:),zg_ip2(:))
colormap(jet)
colorbar