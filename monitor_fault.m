function [s,maxEr,sigEr,minD,d,Time] = monitor_fault(workdir,Data,fault,Ufault)

% Purpose: Calculate characteristic parameter values during fault zone
%          initiation

% Input:
%  workdir  [string] : directory of data
%  Data     [double] : nFault x 2 matrix with start and end of each fault
%  fault    [double] : matrix with fault zone coordidinates
%                      format: [x11 ... x1n;
%                               z11 ... z1n;
%                                   ...
%                               xnFault1 ... xnFaultn;
%                               znFault1 ... znFaultn;
%  Ufault   [double] : nFault vector with migration velocity of each fault

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% MANUAL SETTINGS
FigNo = 456;
if nargin == 3
    Ufault = -0.1; % migration speed of fault zone
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%==========================================================================
%% PREPARATIONS
%==========================================================================
% Scaling factors
scaling  = 'mantle';
NUMSCALE = numerical_eq_scaling(scaling);

% Allocate memory
nData   = max(diff(Data,[],2))+1;
nFaults = length(Ufault);

Time    = NaN(nFaults,nData);
s       = NaN(nFaults,nData); % serpentinization degree
maxEr   = NaN(nFaults,nData); % maximum strain rate
sigEr   = NaN(nFaults,nData); % fault zone width
minD    = NaN(nFaults,nData); % minimum grain size
d       = NaN(nFaults,nData); % mean grain size

%==========================================================================
%% CALCULATIONS
%==========================================================================
% Loop over faults
for iFault = 1:nFaults
    fault_name = ['FAULT' num2str_d(iFault,2)];
    % Create fault zone polygonal line
    ixz = (iFault-1)*2;
    x_fault   = linspace(min(fault(ixz+1,:)),max(fault(ixz+1,:)),50);
    z_fault   = interp1(fault(ixz+1,:),fault(ixz+2,:),x_fault,'pchip');
    
    MESH_F.(fault_name) = create_fault_zones_mesh(x_fault,z_fault);
end

for iFault = 1:nFaults
    fault_name = ['FAULT' num2str_d(iFault,2)];
    dx         = 0;
    % Loop over output files
    for iData = Data(iFault,1):Data(iFault,2)
        
        % Load data
        load([data_storage_2d workdir '/Data_' num2str_d(iData,5)],'VAR','MESH','time')
        
        gc_nod_F      = MESH_F.(fault_name).gc_nod;
        
        % Move fault
        dx          = dx + Ufault(iFault);
        gc_nod_F(1,:) = gc_nod_F(1,:)+dx;
        
        [els,lc] = locate_points_2d(MESH.gc_nod,MESH.el2nod,gc_nod_F);
        loc      = els~=0 & ~any(isnan(lc),1);
        els_loc  = all(ismember(MESH_F.(fault_name).el2nod,find(loc)),1);
        el2nod_F = MESH_F.(fault_name).el2nod(:,els_loc);
        
        % TEST PLOT - Check quality of fault zone fit
        nVert = 3;
        nel   = size(el2nod_F,2); 
        X = reshape(gc_nod_F(1,el2nod_F(1:nVert,:)), nVert, nel);
        Z = reshape(gc_nod_F(2,el2nod_F(1:nVert,:)), nVert, nel);
        plot_2d_fedata(FigNo+1,MESH.gc_nod,MESH.el2nod,log10(VAR.Er_II_totl./NUMSCALE.t0));caxis([-15 -13]);xlim([60 140]);ylim([-35 3])
        figure(FigNo+1)
        hold on
        h = patch(X,Z,nan,'EdgeColor','w','FaceColor','none');
        
        % Map variables to fault zone
        T_fault  = zeros(1,MESH.nnod); T_fault(loc)  = interp2d_tri367(MESH.el2nod(1:3,:),els(loc),lc(:,loc),VAR.T);
        s_fault  = zeros(1,MESH.nnod); s_fault(loc)  = interp2d_tri367(MESH.el2nod(1:3,:),els(loc),lc(:,loc),VAR.serp_degree);
        Er_fault = zeros(1,MESH.nnod); Er_fault(loc) = interp2d_tri367(MESH.el2nod(1:3,:),els(loc),lc(:,loc),VAR.Er_II_totl./NUMSCALE.t0);
        d_fault  = zeros(1,MESH.nnod); d_fault(loc)  = interp2d_tri367(MESH.el2nod(1:3,:),els(loc),lc(:,loc),VAR.grain_size);
        
        % Prescribe ranges for parameter computations
        ind_el_s = any((T_fault(el2nod_F) <= 350),1); % range for serpentinization degree
        ind_d    = (T_fault >= 700 & T_fault <= 1000); % range for grain size
        ind_el_d = any(ind_d,1);
        ind_Er   = find(T_fault >= 700); % range for strain rate
        
        i = iData-Data(iFault,1)+1;
        Time(iFault,i) = time;
        
        % Average serpentinization degree along fault
        s(iFault,i) = FEM_integration_2d(s_fault,gc_nod_F,el2nod_F(:,ind_el_s));
        
        % Max. strain rate
        maxEr(iFault,i) = max(Er_fault(ind_Er));
        % Calculate profile perpendicular to fault + map strain rate
        poly_rect = rect_profile(gc_nod_F,ind_Er(1));
        [els,lc] = locate_points_2d(MESH.gc_nod,MESH.el2nod,poly_rect);
        loc      = els~=0 & ~any(isnan(lc),1);
        Er_rect  = interp2d_tri367(MESH.el2nod(1:3,:),els(loc),lc(:,loc),VAR.Er_II_totl./NUMSCALE.t0);
        % Fit Gaussian curve to strain rate profile and calculate width
        fitG     = fit(poly_rect(1,loc)',Er_rect,'gauss1');
        sigEr(iFault,i) = 1/sqrt(2) * fitG.c1;
        
        % Minimum and mean grain size
        minD(iFault,i)  = min(d_fault(ind_d));
        d(iFault,i) = FEM_integration_2d(d_fault,gc_nod_F,el2nod_F(:,ind_el_d));
    end
    % Remove initial serpentinization degree; TO DO: Consider cutting faults
    s(iFault,:) = s(iFault,:) - min(s(iFault,:));
    
    % Normalize time to:
    % strain rate exceeds 1e-14
    ind_nan = isnan(Time(iFault,:));
    T0      = interp1(maxEr(iFault,~ind_nan),Time(iFault,~ind_nan),1e-14);
    % width starts to decrease
%     [~,ind_Norm] = max(sigEr(iFault,:));
%     T0 = Time(iFault,ind_Norm);
    Time(iFault,:) = Time(iFault,:) - T0;
end

% and move to zero
Time = Time - min(Time(:));

% Calculate mean over all faults
% Create time line
dt = 0.01; % [Myr]
Time0 = 0:dt:max(Time(:));
s0    = nan(nFaults,length(Time0));
maxEr0 = nan(nFaults,length(Time0));
sigEr0 = nan(nFaults,length(Time0));
minD0  = nan(nFaults,length(Time0));
d0     = nan(nFaults,length(Time0));

figure(FigNo);clf
for iFault = 1:nFaults
    
    ind_nan = isnan(Time(iFault,:));
    
    % Interpolate fault data to reference time line
    s0(iFault,:)     = interp1(Time(iFault,~ind_nan),s(iFault,~ind_nan),Time0);
    maxEr0(iFault,:) = interp1(Time(iFault,~ind_nan),maxEr(iFault,~ind_nan),Time0);
    sigEr0(iFault,:) = interp1(Time(iFault,~ind_nan),sigEr(iFault,~ind_nan),Time0);
    minD0(iFault,:)  = interp1(Time(iFault,~ind_nan),minD(iFault,~ind_nan),Time0);
    d0(iFault,:)     = interp1(Time(iFault,~ind_nan),d(iFault,~ind_nan),Time0);
    
    %======================================================================
    %% PLOT RESULTS
    %======================================================================
    figure(FigNo);
    subplot(4,1,1)
    hold on
    plot(Time0,s0(iFault,:),'color',[0.8 0.8 0.8],'linewidth',2)
    subplot(4,1,2)
    hold on
    plot(Time0,log10(maxEr0(iFault,:)),'color',[0.8 0.8 0.8],'linewidth',2)
    subplot(4,1,3)
    hold on
    plot(Time0,sigEr0(iFault,:),'color',[0.8 0.8 0.8],'linewidth',2)
    subplot(4,1,4)
    hold on
    plot(Time0,minD0(iFault,:),'color',[1 0.95 0.8],'linewidth',2)
    hold on
    plot(Time0,d0(iFault,:),'color',[1 0.8 1],'linewidth',2)
end

line_col = lines(5);

figure(FigNo)
subplot(4,1,1)
hold on
plot(Time0,mean(s0,1),'-','linewidth',1.5,'color',line_col(5,:))
ylabel('Serpentinization degree')
subplot(4,1,2)
plot(Time0,mean(log10(maxEr0),1),'-','linewidth',1.5,'color',line_col(2,:))
ylabel('Max. strain rate [s^{-1}]')
subplot(4,1,3)
plot(Time0,mean(sigEr0,1),'-','linewidth',1.5,'color','k')
ylabel('Fault zone width [km]')
subplot(4,1,4)
d1 = plot(Time0,mean(minD0,1),'-','linewidth',1.5,'color',line_col(3,:));
hold on
d2 = plot(Time0,mean(d0,1),'-','linewidth',1.5,'color',line_col(4,:));
legend([d1,d2],{'minimum','mean'})
ylabel('Grain size [micron]')
xlabel('Time [Myr]')

end % END OF FUNCTION monitor_faults

%==========================================================================
%% SUBFUNCTIONS
%==========================================================================
function MESH = create_fault_zones_mesh(x_fault,z_fault)
% triangle options
opts.element_type     = 'tri3';
opts.min_angle        = 30;
opts.max_tri_area     = 0.3^2;

width = 4;

fault_up   = [x_fault+width/2;z_fault];
fault_down = fliplr([x_fault-width/2;z_fault]);

tristr.points         = [fault_up fault_down];
tristr.segments = uint32([1:2*length(x_fault); 2:2*length(x_fault) 1]);
MESH_tmp = mtriangle(opts, tristr);
MESH.gc_nod = MESH_tmp.NODES;
MESH.el2nod = MESH_tmp.ELEMS;
MESH.nel    = size(MESH.el2nod,2);
MESH.nnod   = size(MESH.gc_nod,2);
end

function I = FEM_integration_2d(var_nod,gc_nod,el2nod)
nip    = 1;
nnodel = size(el2nod,1);
nel    = size(el2nod,2);

[x_ip,w_ip] = ip_triangle(nip);
[N,dN]    = sf_dsf_tri367(x_ip,nnodel,'cell');

A = 0;
I = 0;
for iel = 1:nel
    inod = el2nod(:,iel);
    for ip = 1:nip
        var_ip = var_nod(el2nod)' * N{ip};
        
        %==============================================================
        % CALCULATE JACOBIAN
        %==============================================================
        J = dN{ip}*gc_nod(:,inod)';
        
        A       = A+0.5*det(J);
        I       = I + var_ip(iel,ip)*det(J)*w_ip(ip);
    end
end
I = I/A;
end

function I = int_poly(var,poly,ind)

if nargin == 2
    ind = true(size(var));
end
var  = var(ind);
poly = poly(:,ind(1:end));

Lseg = sqrt((poly(1,2:end)-poly(1,1:end-1)).^2 + (poly(2,2:end)-poly(2,1:end-1)).^2);

I = 1/sum(Lseg) .* sum((var(1:end-1)+var(2:end))/2 .* Lseg');
end

function poly_rect = rect_profile(poly,ind)

L = 10;
try
dx = poly(1,ind+1)-poly(1,ind);
dz = poly(2,ind+1)-poly(2,ind);
catch
end
Phi = atan(dz/dx)+pi/2;

x0 = poly(1,ind) - cos(Phi)*L;
x1 = poly(1,ind) + cos(Phi)*L;
z0 = poly(2,ind) - sin(Phi)*L;
z1 = poly(2,ind) + sin(Phi)*L;

poly_rect = [linspace(x0,x1,100);linspace(z0,z1,100)];
end