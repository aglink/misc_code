function serp_GSR_postproc(workdir,Data_list,FigNo)

fontsize = 18;

xlimits = [-50 50];
zlimits = [-40 12];

path2data = data_storage_2d;
if nargin == 0
    Data_list = 271;
    workdir = '/Download_Uniserver/MA_RefRun_steadyT_G30GPa/';
    FigNo   = 555;
end
load([path2data workdir '/SETUP.mat'])

for Data = Data_list

load([path2data workdir '/Data_' num2str_d(Data,5)])
    
gc_nod = MESH.gc_nod;
inod_top = find(ismember(MESH.PointID,MESH.PointID_top));
[x_sort,ind] = sort(MESH.gc_nod(1,inod_top)-100);
minZ = min(MESH.gc_nod(2,inod_top));
gc_nod(1,:) = gc_nod(1,:)-100;
gc_nod(2,:) = gc_nod(2,:);%-minZ-5;

% figure(FigNo+100)
% plot(x_sort,gc_nod(2,inod_top(ind)),'k','linewidth',3)
% xlim([-100 100])
% ylim([-7 4])
% yticks(-6:1:3)
% xlabel('x [km]')
% ylabel('z [km]')
% set(gca,'Fontsize',20,'FontName','Times New Roman')

MESH.gc_nod = gc_nod;
SETTINGS.save_figs = 0;
SETTINGS.show_figs = 1;
SETTINGS.vars_plot_clim{end}(1) = 5000;
SETTINGS.use_tracer = 0;
plot_serp_GSR_POSTPROC(VAR,MESH,PHYSICS,SETTINGS,TC,[-50 50],[-40 10],time,77,FigNo+1)
hold on
plot(x_sort,gc_nod(2,inod_top(ind)),'k','linewidth',1)
if ~isempty(xlimits)
    xlim(xlimits)
end
if ~isempty(zlimits)
    ylim(zlimits)
end
xticks(-100:10:100)
xlabel('x [km]')
ylabel('z [km]')
% title(sprintf('%.2f Myr',time),'FontSize',fontsize);
set(gca,'Fontsize',fontsize,'FontName','Times New Roman')

isotherms = [350 800 1000];

for i = 1:length(isotherms)
    els_iso = any(VAR.T(MESH.el2nod(1:6,:)) >= isotherms(i)) & any(VAR.T(MESH.el2nod(1:6,:)) < isotherms(i));
    x_el = MESH.gc_nod(1,MESH.el2nod(7,els_iso));
    z_el = MESH.gc_nod(2,MESH.el2nod(7,els_iso));
    [~,ind] = sort(x_el);
    
%     figure(FigNo)
%     plot(smooth(x_el(ind),10),smooth(z_el(ind),10),':w','linewidth',3)
    figure(FigNo+2)
    plot(smooth(x_el(ind),10),smooth(z_el(ind),10),':w','linewidth',3)
end

pause(1)

print(FigNo+2,'-dpng','-r600',[data_storage_2d workdir '/POSTPROC/Plots/SerpGSR_isoT_' num2str_d(Data,5)]);
%  print(FigNo+2,'-dpng','-r600',[data_storage_2d workdir '/POSTPROC/SerpGSR_isoT_' num2str_d(Data,5)]);
end
 